import React, {useReducer} from 'react';
import {BrowserRouter} from 'react-router-dom';

import Info from "./Components/Info/Info";
import Admin from "./Components/Admin/Admin";
import User from "./Components/User/User";
import Home from "./Components/Home/Home";
import Navbar from "./Components/Navbar";

import Context from './utils/context';
import * as ACTION from "./actionCreators/action"
import {productReducer, defaultProductState} from "./reducers/product.reducer"
import {infoReducer, defaultUserState} from "./reducers/info.reducer"

import { Route } from "react-router-dom";

function App() {

const [stateProduct, dispatchProductReducer] = useReducer(productReducer, defaultProductState);
  const findProduct = (data) => {
    dispatchProductReducer(ACTION.findProduct(data));
  };
  const addProduct = (data) => {
    dispatchProductReducer(ACTION.addProduct(data));
  };
  
  const editProduct = (data) => {
    dispatchProductReducer(ACTION.editProduct(data));
  }
  const deleteProduct = (id) => {
    dispatchProductReducer(ACTION.deleteProduct(id));
  }

  const [stateUser, dispatchUsertReducer] = useReducer(infoReducer, defaultUserState)
  const editUser = (data) => {
    dispatchUsertReducer(ACTION.editUser(data));
  }
  
    return (
        
        <BrowserRouter>
            <Navbar />
            <Route path='/home' component={Home}/>
            <Context.Provider
            value={{
                productState : stateProduct,
                findProduct,
                addProduct,
                editProduct,
                deleteProduct
            }}
            >
                <Route path='/admin' component={Admin}/>
                <Route path='/user' component={User}/>
            </Context.Provider>
            
            <Context.Provider
            value={{
                userState: stateUser,
                editUser,
            }}
            >
                <Route path='/info' component={Info}/>
            </Context.Provider>
        </BrowserRouter>
        
    );
}

export default App;
