import React, {useState, useContext} from 'react';
import Context from '../../utils/context';

function User(props) {
    const context = useContext(Context);
    const [name, setName] = useState(context.productState.name);
    const handleFind = () => {
        context.findProduct(name)
    }

        return (
            <div className="User">
                <div className="User_text">Навигация по имени</div>
                <textarea className="User_textarea" value={name} onChange={(e)=>setName(e.target.value)}></textarea>
                <div className="Search_buttons">
                    <button onClick={handleFind}>Поиск</button>
                </div>            
                <div className = "Product_block">
                    {context.productState.map(product => (
                            <div className="Product"> 
                                <div className="Product_obert">
                                    <div className="Product_title">{product.name}</div>
                                    <img className="Product_img" src = {product.img} alt = "картинка"></img>
                                    <div className="Product_discription">{product.discription}</div>
                                    <div className="Product_price">{product.price}</div> 
                                    <div className="Product_buttons">
                                        <button>Купить</button>
                                    </div> 
                                </div>  
                            </div>
                        ))}
                </div>
           </div>
        )
}

export default User;
