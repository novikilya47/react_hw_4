import React, {useState, useContext} from 'react';
import Context from '../../utils/context';

function Admin(props) {
    const [name, setName] = useState();
    const [id, setId] = useState();
    const [img, setImg] = useState();
    const [discription, setDescription] = useState();
    const [price, setPrice] = useState();
    const [isEdit, setIsEdit] = useState(false);
    const context = useContext(Context);

    const sendNewProduct = () => {
        let product = {
            id: isEdit ? id : context.productState.length+1,
            name: name,
            img: img,
            discription: discription,
            price: price
        };

        if(isEdit) {
            context.editProduct(product);
        } else {
            context.addProduct(product);
        }
        setIsEdit(false);
    }

    const handleEdit = (product) => {
        setIsEdit(true);
        setId(product.id);
        setName(product.name);
        setImg(product.img);
        setDescription(product.discription);
        setPrice(product.price);
    }

    const handleDelete = (product) => {
        context.deleteProduct(product.id)
    }
        
        return (
            <div className = "Admin">
                <div className = "Admin_block">
                    <div>Форма добавления и изменения товара</div>
                    <div className = "Admin_block_field"><p>Наименование товара: </p><textarea value={name} onChange={(e) => setName(e.target.value)}/></div>
                    <div className = "Admin_block_field"><p>Ссылка на изображение: </p><textarea value={img} onChange={(e) => setImg(e.target.value)}/></div>
                    <div className = "Admin_block_field"><p>Описание товара: </p><textarea value={discription} onChange={(e) => setDescription(e.target.value)}/></div>
                    <div className = "Admin_block_field"><p>Цена: </p><textarea value={price} onChange={(e) => setPrice(e.target.value)}/></div>
                    <div className="State_buttons">
                        <button onClick={sendNewProduct}>Отправить</button>
                    </div>
                    
                </div>
                <div className = "Product_block">
                    {context.productState.map(product => (
                            <div className="Product"> 
                                <div className="Product_obert">
                                    <div className="Product_title">{product.name}</div>
                                    <img className="Product_img" src = {product.img} alt = "картинка"></img>
                                    <div className="Product_discription">{product.discription}</div>
                                    <div className="Product_price">{product.price}</div>  
                                    <div className="Product_buttons">
                                        <button onClick={() => handleEdit(product)}>Изменить</button>
                                        <button onClick={() => handleDelete(product)}>Удалить</button>
                                    </div> 
                                </div>
                            </div>
                        ))}
                </div>
            </div>
        )
}

export default Admin;
