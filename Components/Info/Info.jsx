import React, {useState, useContext} from 'react';
import Context from '../../utils/context';;

function Info(props) {
    const [name, setName] = useState();
    const [email, setEmail] = useState();
    const context = useContext(Context);

    const handleEdit = () => {
        let user = {
            name: name ? name : context.userState.name,
            email: email ? email : context.userState.email
        };
        context.editUser(user);
    }
    
    return (
        <div className="Info">
            <div>Имя: {context.userState.name}</div>
            <div>Email: {context.userState.email}</div>
            <div className="Info_inputs">
                <div className="Info_input"><p>Имя</p><textarea type='text' onChange={(e) => setName(e.target.value)}/></div>
                <div className="Info_input"><p>Email</p><textarea type='text' onChange={(e) => setEmail(e.target.value)}/>  </div>
            </div>  
            <button onClick={() => handleEdit()}>Изменить информацию</button>   
        </div>
    )
}

export default Info;
