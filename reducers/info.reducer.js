import {EDIT_USER} from "../Constants/constants";
export const defaultUserState = {name: "Ilya", email : "novikilya@mail.ru"}

export function infoReducer (state = defaultUserState, action){
    switch (action.type) {
        case EDIT_USER:
            let newState = {
                name: action.data.name,
                email: action.data.email,
            }
            return newState;
        default:
            return state;
    }
};
